/**
 * @author      : oli (oli@oli-HP)
 * @file        : func
 * @created     : Wednesday Nov 16, 2022 11:38:32 GMT
 */

#ifndef FUNC_HPP
#define FUNC_HPP

int add(int i, int j);

#endif // end of include guard FUNC_HPP 

