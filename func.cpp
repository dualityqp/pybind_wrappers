/**
 * @author      : oli (oli@oli-HP)
 * @file        : func
 * @created     : Wednesday Nov 16, 2022 11:30:23 GMT
 */


int add(int i, int j)
{
    return i + j;
}
// #include <pybind11/pybind11.h>

// namespace  py = pybind11;

// PYBIND11_MODULE(example, m)
// {
//     m.def("add", &add);
// }

