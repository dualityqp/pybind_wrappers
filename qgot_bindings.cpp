/**
 * @author      : oli (oli@oli-HP)
 * @file        : qgot_bindings
 * @created     : Wednesday Nov 16, 2022 14:57:55 GMT
 */

#include <pybind11/pybind11.h>

#include <qgot_public/math-code/state.hpp>

namespace  py = pybind11;

PYBIND11_MODULE(pyqgot, m)
{
    py::class_<QGot::State>(m, "State")
        .def(py::init<size_t>())
        .def("print", &QGot::State::print);
}




