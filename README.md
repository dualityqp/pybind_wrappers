# pybind_wrappers

## REQUIRES 

```bash
cmake 
```

```bash
pybind11-dev
```

### On Ubuntu

```bash
sudo apt-get update
sudo apt-get install -y cmake pybind11-dev
```

## To build the python library

Make sure a build directory exists 
```bash
mkdir build
```

```bash
cd build 
cmake ..
cmake --build . 
```

Then in python you can 
```python
import olispylib as oli
oli.add(3,4)
```

See `main.py` for a python calling example
